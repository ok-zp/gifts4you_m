/**
 * Created by Sunny on 30.11.2016.
 */
var gulp = require('gulp');
var less = require('gulp-less');
//var minify = require('gulp-minify');
//var minify = require('gulp-clean-css');
//var rename = require('gulp-rename');
var watch = require('gulp-watch');
var notify = require('gulp-notify');
//var uglify = require('gulp-uglify');
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;


gulp.task('default', ['browsersync', 'watch']);

gulp.task('browsersync', function () {
    browserSync.init({
        server: {
            baseDir: "app/"
        },
        port: 8080,
        open: true,
        notify: false
    });
});

gulp.task('style', function () {
    return gulp.src("src/less/main.less", {style: 'expanded'})
        .pipe(less({includePaths: ['src/less/**']}))
        //.pipe(rename({suffix: '.min'}))
        //.pipe(minify())
        .pipe(gulp.dest('app/web/css/'))
        .pipe(notify({message: 'Style task is finished'}))
        .pipe(reload({stream: true}));

});

//gulp.task('script', function () {
 //   return gulp.src("src/js/**/*.js", {style: 'expanded'})
       // .pipe(uglify())
       // .pipe(rename({suffix: '.min'}))
      //  .pipe(gulp.dest('js/'))
      //  .pipe(notify({message: 'Scripts task is finished'}))
     //   .pipe(reload({stream: true}));
//});

//// ??????????
gulp.task('reload', function () {
    return reload({stream: true});
});

////////////////////
gulp.task('watch', function () {
    gulp.watch("src/less/**/*.less", ['style']);
    //gulp.watch("src/js/**/*.js", ['script']);
   // gulp.watch("index.html", ['reload']);
    gulp.watch("index.html").on("change", reload);
    gulp.watch("app/views/site/**/*.html").on("change", reload);

});