<?php

namespace app\controllers;

use app\models\Category;
use app\models\Complaint;
use app\models\ProductDeliveryType;
use app\models\DeliveryType;
use app\models\Faq;
use app\models\FeedbackClient;
use app\models\FeedbackShop;
use app\models\Holiday;
use app\models\Product;
use app\models\Shop;
use app\models\Translations;

class ApiController extends \yii\web\Controller
{
    public function beforeAction($action)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return parent::beforeAction($action);
    }

    public function behaviors()
    {
        return [
            'corsFilter' => [
                'class' => \yii\filters\Cors::className(),
                'cors' => [
                    // restrict access to
                    'Origin' => ['*'],
                ],

            ],
        ];
    }

    public function actionFaq()
    {
        $request = \Yii::$app->request;

        $client = $request->get('is_client');
        if (!isset($client))
            throw new \RuntimeException("u forgot to send client param");

        $faq_model = Faq::find()->where(['client' => $client, 'lang' => $this->getCurrentLanguage()])->all();
        return $faq_model;
    }

    public function actionAdvice()
    {
        $advices = Translations::find()->where(['status' => '1', 'lang' => $this->getCurrentLanguage()])->all();
        return $advices;
    }

    public function getCurrentLanguage()
    {
        $cookies = \Yii::$app->request->cookies;
        $language = $cookies->getValue('lang', 'ru');

        return $language;
    }

    public function getUserId()
    {
        $cookies = \Yii::$app->request->cookies;
        $user_id = $cookies->getValue('user_id', '1');

        return $user_id;
    }

    public function actionRegistration()
    {
        $request = \Yii::$app->request;

        $email = $request->get('email');
        $login = $request->get('login');
        $password = $request->get('password');
        $confirmPassword = $request->get('confirmPassword');
        $response = [$email, $login, $password, $confirmPassword];
        return $response;
    }

    public function actionLogin()
    {
        $request = \Yii::$app->request;

        $login = $request->get('login');
        $password = $request->get('password');

        $response = [$login, $password];
        return $response;
    }

    public function actionFeedbackshop()
    {
        $request = \Yii::$app->request;
        $model = new FeedbackShop();

        //$id = $request->get('user_id');
        $id = $this->getUserId();
        $name = $request->get('name');
        $subject = $request->get('subject');
        $text = $request->get('text');

        $model->user_id = $id;
        $model->name = $name;
        $model->subject = $subject;
        $model->body = $text;

        if ($model->save()) {
            return true;
        } else {
            return $model->getErrors();
        }

    }

    public function actionFeedbackclient()
    {
        $request = \Yii::$app->request;
        $model = new FeedbackClient();

        $name = $request->get('name');
        $email = $request->get('email');
        $subject = $request->get('subject');
        $text = $request->get('text');

        $model->name = $name;
        $model->email = $email;
        $model->subject = $subject;
        $model->body = $text;

        if ($model->save()) {
            return true;
        } else {
            return $model->getErrors();
        }

    }

    public function actionClosestholidays()
    {

        $closestHolidays = Holiday::find()
            ->where("(date - INTERVAL notifyPeriod DAY <= DATE(NOW()) AND DATE(NOW()) <= date) OR notifyPeriod = 0")
            ->orderBy("date")
            ->limit(15)
            ->all();

        return $closestHolidays;

    }

    public function actionCategories()
    {

        $categories = Category::find()->all();

        return $categories;

    }

    public function actionProducts()
    {
        $products = Product::find()->where(['status' => '1', 'deleted' => '0'])->all();
        return $products;
    }

    public function actionProduct()
    {
        $request = \Yii::$app->request;

        $id = $request->get('product_id');

        //  $product = Product::find()->onlyActive()->where(['status' => 1, 'deleted' => 0])->byPk($id)->all();
        //TODO
        $product = Product::find()->where(['id' => $id, 'status' => 1, 'deleted' => 0])->one();
        $shop = Shop::find()->where(['id' => $product->shop_id])->one();
        // $complaint = Complaint::find()->where(['product_id' => $product->id, 'deleted' => '0'])->all();

        $types = ProductDeliveryType::find()->where(['product_id' => $id])->all();


        $deliveryTypesArray[] = null;

        for ($i = 0; $i < count($types); $i++) {
            $type_id = $types[$i]->delivery_type_id;
            $type = DeliveryType::find()->where(['id' => $type_id])->one();
            if (($type->type) != null) {
                $deliveryTypesArray[$type_id] = "$type->type";
            }
        }


        if ($shop->status != 1 || $product->status != 1) {
            //TODO  не находит этот класс в папке с фреймворком
            // throw new CHttpException (404, 'The requested page does not exist.');
        }
        $data = [];
        $data["product"] = $product;
        $data["shop"] = $shop;
        $data["deliveryTypes"] = $deliveryTypesArray;
        return $data;
    }

    public function actionClosestholidaysprofessional()
    {

        $closetHolidaysProfessional = Holiday::find()
            ->where("date >= CURDATE() AND holiday_type_id = 2")
            ->limit(2)
            ->all();

        return $closetHolidaysProfessional;

    }

    public function actionClosestholidaysnormal()
    {

        $closetHolidaysNormal = Holiday::find()
            ->where("date >= CURDATE() AND holiday_type_id = 0")
            ->limit(1)
            ->all();

        return $closetHolidaysNormal;

    }

    public function actionClosestholidaysreligious()
    {

        $closetHolidaysReligious = Holiday::find()
            ->where("date >= CURDATE() AND holiday_type_id = 1")
            ->limit(2)
            ->all();

        return $closetHolidaysReligious;

    }

}
