'use strict';

angular.module('gifts4you', ['ui.router', "ngSanitize"])
    .config(routesConfig);

function routesConfig($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/");
    $stateProvider
        .state('index', {
            url: '/',
            templateUrl: '/views/site/index.tpl.html',
            controller: 'IndexController',
            controllerAs: 'vm'
        })
        .state("about", {
            url: '/about',
            templateUrl: "/views/site/about.tpl.html",
            controller: 'AboutController',
            controllerAs: 'vm'
        })
        .state("about_cooperation", {
            url: '/about_cooperation',
            templateUrl: "/views/site/about.tpl.html",
            controller: 'AboutCooperationController',
            controllerAs: 'vm'
        })
        .state("faq", {
            url: '/faq',
            templateUrl: "/views/site/faq.tpl.html",
            controller: 'FaqController',
            controllerAs: 'vm'
        })
       .state("faq_shop", {
            url: '/faq_shop',
            templateUrl: "/views/site/faq.tpl.html",
            controller: 'FaqShopController',
            controllerAs: 'vm'
        })
        .state("advice", {
            url: '/advice',
            templateUrl: "/views/site/advice.tpl.html",
            controller: 'AdviceController',
            controllerAs: 'vm'
        })
        .state("login", {
            url: '/login',
            templateUrl: "/views/site/login.tpl.html",
            controller: 'LoginController',
            controllerAs: 'vm'
        })
        .state("registration", {
            url: '/registration',
            templateUrl: "/views/site/registration.tpl.html",
            controller: 'RegistrationController',
            controllerAs: 'vm'
        })
        .state("feedbackClient", {
            url: '/feedback_client',
            templateUrl: "/views/site/feedback_client.tpl.html",
            controller: 'FeedbackClientController',
            controllerAs: 'vm'
        })
        .state("feedbackShop", {
            url: '/feedback_shop',
            templateUrl: "/views/site/feedback_shop.tpl.html",
            controller: 'FeedbackShopController',
            controllerAs: 'vm'
        })
        .state("products", {
            url: '/products',
            templateUrl: "/views/site/products.tpl.html",
            controller: 'ProductsController',
            controllerAs: 'vm'
        })
        .state("product", {
            url: '/product',
            templateUrl: "/views/site/product.tpl.html",
            controller: 'ProductController',
            controllerAs: 'vm'
        });

}