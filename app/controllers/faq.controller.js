
(function () {

    'use strict';

    var app = angular
        .module('gifts4you')
        .controller('FaqController', ['$scope', 'FaqService', FaqController]);

    function FaqController($scope, FaqService) {

        $scope.url = document.location.href;
        FaqService.getFaqClient().then(function (data) {
            $scope.faqs = data;
        });

    }
})();