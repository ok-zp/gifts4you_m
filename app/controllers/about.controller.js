
(function () {

    'use strict';

    var app = angular
        .module('gifts4you')
        .controller('AboutController', ['$scope', 'AboutService', AboutController]);

    function AboutController($scope, AboutService) {

        AboutService.getAbout().then(function (data) {

            $scope.about = data;
        });

    }
})();