
(function () {

    'use strict';

    var app = angular
        .module('gifts4you')
        .controller('HolidaysController', ['$scope', 'HolidaysService', HolidaysController]);

    function HolidaysController($scope, HolidaysService) {

        HolidaysService.getClosestHolidays().then(function (data) {
            $scope.closestHolidays = data;
        });
        HolidaysService.getProfessionalHolidays().then(function (data) {
            $scope.closestHolidaysProfessional = data;
        });
        HolidaysService.getNormalHolidays().then(function (data) {
            $scope.closestHolidaysNormal = data;
        });
        HolidaysService.getReligiousHolidays().then(function (data) {
            $scope.closestHolidaysReligious = data;
        });
    }
})();