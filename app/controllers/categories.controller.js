
angular.module('gifts4you')
    .controller('CategoriesController',
    function ($scope, $http) {

        $http.get('/data/categories.json').success(function (data) {
            $scope.categories = data;
        });

    });
