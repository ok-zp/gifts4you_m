
(function () {

    'use strict';
    var MAX_LEN = 200;
    var WARN_LEN = 10;
    var app = angular
        .module('gifts4you')
        .controller('FeedbackClientController', ['$scope', 'FeedbackService', FeedbackClientController]);

    function FeedbackClientController($scope, FeedbackService) {


        $scope.user = {
            text:""
        };

        $scope.remaining = function () {

            return MAX_LEN - $scope.feedback_client.text.$viewValue.length;
        };
        $scope.shouldWarn = function () {
            console.log('warn');
            console.log($scope.remaining());
            return WARN_LEN > $scope.remaining();
        };


        $scope.submitForm = function () {

            // check to make sure the form is completely valid
            if ($scope.feedback_client.$valid) {
                alert('form has been sent');
            }

        }


    }
})();