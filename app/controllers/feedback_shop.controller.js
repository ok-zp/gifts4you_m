
(function () {

    'use strict';
    var MAX_LEN = 200;
    var WARN_LEN = 10;
    var app = angular
        .module('gifts4you')
        .controller('FeedbackShopController', ['$scope', 'FeedbackService', FeedbackShopController]);

    function FeedbackShopController($scope, FeedbackService) {

        $scope.user = {
            text:""
        };

        $scope.remaining = function () {
            // console.log($scope.feedback_client.text);
            return MAX_LEN - $scope.feedback_shop.text.$viewValue.length;
        };
        $scope.shouldWarn = function () {
            console.log('warn');
            console.log($scope.remaining());
            return WARN_LEN > $scope.remaining();
        };


        $scope.submitForm = function () {

            // check to make sure the form is completely valid
            if ($scope.feedback_shop.$valid) {
                alert('form has been sent');
            }

        }


    }
})();

