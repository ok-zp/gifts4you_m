

angular.module('gifts4you')
    .controller('ProductsController',
    function ($scope, $http) {

        $http.get('data/products.json').success(function (data) {
            $scope.products = data;
        });

    });
