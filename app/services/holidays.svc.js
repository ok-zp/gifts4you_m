
(function () {
    'use strict';
    angular
        .module('gifts4you')
        .factory('HolidaysService', ['$http', HolidaysService]);

    function HolidaysService($http, $window) {

        return {
            getClosestHolidays: getClosestHolidays,
            getProfessionalHolidays: getProfessionalHolidays,
            getNormalHolidays: getNormalHolidays,
            getReligiousHolidays: getReligiousHolidays
        };

        function getClosestHolidays() {
            return  $http.get('/data/closest_holidays.json').then(function(res){
                return res.data;
            });
        }

        function getProfessionalHolidays() {
            return  $http.get('/data/closest_holidays_professional.json').then(function(res){
                return res.data;
            });
        }

        function getNormalHolidays() {
            return  $http.get('/data/closest_holidays_normal.json').then(function(res){
                return res.data;
            });
        }

        function getReligiousHolidays() {
            return  $http.get('/data/closest_holidays_religious.json').then(function(res){
                return res.data;
            });
        }


    }
})();