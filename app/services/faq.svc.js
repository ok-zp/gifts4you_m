
(function () {
    'use strict';
    angular.module('gifts4you')
        .factory('FaqService', ['$http', FaqService]);

    function FaqService($http, $window) {

        return {
            getFaqClient: getFaqClient,
            getFaqShop: getFaqShop

        };

        function getFaqClient() {
            return  $http.get('data/faq_client.json').then(function(res){
                return res.data;
            });
        }

        function getFaqShop() {
            return  $http.get('data/faq.json').then(function(res){
                return res.data;
            });
        }


    }
})();