
(function () {
    'use strict';
    angular.module('gifts4you')
        .factory('AboutService', ['$http', AboutService]);

    function AboutService($http, $window) {

        return {
            getAbout: getAbout,
            getAboutCooperation: getAboutCooperation


        };

        function getAbout() {
            return $http.get('data/about.json').then(function (res) {
                return res.data;
            });
        }

        function getAboutCooperation() {
            return $http.get('data/about_cooperation.json').then(function (res) {
                return res.data;
            });
        }


    }
})();