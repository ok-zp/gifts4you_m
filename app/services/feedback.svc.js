(function () {
    'use strict';
    angular.module('gifts4you')
        .factory('FeedbackService', ['$http', "$q", FeedbackService]);

    function FeedbackService($http, $q ) {

        return {
            postFeedbackShop: postFeedbackShop,
            postFeedbackClient: postFeedbackClient

        };

        function postFeedbackShop(feedback) {
            // perform some asynchronous operation, resolve or reject the promise when appropriate.
            return $q(function(resolve, reject) {
                setTimeout(function() {
                    if (feedback.name.length>0) {
                        resolve(true);
                    } else {
                        reject(false);
                    }
                }, 1000);
            });
        }

        function postFeedbackClient(feedback) {
            // perform some asynchronous operation, resolve or reject the promise when appropriate.
            return $q(function(resolve, reject) {
                setTimeout(function() {
                    if (feedback.name.length>0) {
                        resolve(true);
                    } else {
                        reject(false);
                    }
                }, 1000);
            });
        }


    }
})();